import { useEffect } from "react";
import axios from "axios";
import PropTypes from "prop-types";

const Login = ({ onLogin }) => {
  const envConfig = {
    tspaBaseUrl: import.meta.env.VITE_TSPA_BASE_URL,
    authBaseUrl: import.meta.env.VITE_AUTHORIZATION_BASE_URL,
    redirectUrl: import.meta.env.VITE_REDIRECT_URL,
    clientId: import.meta.env.VITE_CLIENT_ID,
    clientSecret: import.meta.env.VITE_CLIENT_SECRET,
    realm: import.meta.env.VITE_REALM,
    roleAllowed: import.meta.env.VITE_ROLE_ALLOWED,
  };

  useEffect(() => {
    const urlSearchParams = new URLSearchParams(window.location.search);
    const code = urlSearchParams.get("code");
    if (code) {
      axios
        .post(`${envConfig.tspaBaseUrl}/api/authorize`, { code, envConfig })
        .then((response) => {
          onLogin(response.data.allowed);
        })
        .catch((error) => {
          console.error("Error calling backend", error);
        });
    }
  });

  const handleAccessClick = async () => {
    console.log(
      `Auth base URL from .env: ${
        import.meta.env.VITE_TSPA_BASE_URL
      }/api/authenticate`
    );
    try {
      axios
        .post(`${envConfig.tspaBaseUrl}/api/authenticate`, { envConfig })
        .then((res) => {
          const redirectUrl = res.data.authUrl;
          if (redirectUrl) {
            window.location.href = redirectUrl;
          }
        })
        .catch((error) => {
          console.error("Error calling backend: ", error);
        });
    } catch (error) {
      console.error("Error fetching redirect URL", error);
    }
  };

  return (
    <div className="container-login">
      <div className="column-picture">
        <img src="/images/background_waves.png" className="full-width" />
      </div>
      <div className="column form centered-content">
        <img src="/images/logo.svg" />
        <h1 className="login-hero-title layout">
          Welcome to the Trust Framework configuration tool
        </h1>
        <button className="outline-button" onClick={handleAccessClick}>
          access
        </button>
      </div>
    </div>
  );
};

Login.propTypes = {
  onLogin: PropTypes.func.isRequired,
};

export default Login;
