import express from 'express';
import ViteExpress from 'vite-express';
import cors from 'cors';
import 'dotenv/config';
import router from './router.js';

const app = express();
app.use(cors());
app.use(express.json());
app.use('/api', router);

// const port = import.meta.env.VITE_NODE_ENV === 'production' ? 80 : 8080;
// const port = 8080;
const port = process.env.NODE_ENV === 'production' ? 80 : 8080
ViteExpress.listen(app, port, () =>
  console.log(`Server is listening on port ${port}...`),
);


// const express = require('express')
// const ViteExpress = require('vite-express')
// const cors = require("cors")
// require('dotenv').config()


// import express from 'express';
// import cors from 'cors'
// import 'dotenv/config';

// const app = express();
// app.use(cors());
// app.use(express.json());
// app.use("/api", require('./router'));

// const port = process.env.NODE_ENV === 'production' ? 80 : 8080

// ViteExpress.listen(app, port, () =>
//   console.log(`Server is listening on port ${port}...`),
// );
