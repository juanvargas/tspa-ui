import express from "express";
import "dotenv/config";
import axios from "axios";
import jwt from "jsonwebtoken";

const router = express();
let bearer_token;
let refresh_token;

router.get("/hello", (req, res) => {
  res.send("Hello");
});

// Generates authentication url to redirect the user to IdP login page. Called when user clicks "access"
router.post("/authenticate", (req, res) => {
  const envConfig = req.body.envConfig;
  const params = new URLSearchParams();
  params.append("client_id", envConfig.clientId);
  params.append("redirect_uri", envConfig.redirectUrl);
  params.append("scope", "openid");
  params.append("response_type", "code");

  const authenticateURL = new URL(
    `${envConfig.authBaseUrl}/auth/realms/${envConfig.realm}/protocol/openid-connect/auth`
  );
  authenticateURL.search = params;
  console.log(
    "generating url to transfer the user (to IdP) for login: " +
      authenticateURL.href
  );
  res.json({ authUrl: authenticateURL.href });
});

// Get access/refresh tokens from IdP to authorize user. It's the callback from IdP
router.post("/authorize", (req, res) => {
  const authorizationCode = req.body.code;
  const envConfig = req.body.envConfig;
  if (authorizationCode === undefined) {
    res.json({
      message: "No auth code provided",
    });
  }
  const tokenEndpoint = `${envConfig.authBaseUrl}/auth/realms/${envConfig.realm}/protocol/openid-connect/token`;
  const redirectUri = envConfig.redirectUrl;

  const data = new URLSearchParams();
  data.append("grant_type", "authorization_code");
  data.append("client_id", envConfig.clientId);
  data.append("client_secret", envConfig.clientSecret);
  data.append("code", authorizationCode);
  data.append("scope", "openid");
  data.append("redirect_uri", redirectUri);
  let allowed;

  axios
    .post(tokenEndpoint, data, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
    .then((response) => {
      const decodedToken = jwt.decode(response.data.access_token);
      bearer_token = response.data.access_token;
      refresh_token = response.data.refresh_token;
      allowed = decodedToken.realm_access.roles.includes(envConfig.roleAllowed);
      res.json({ allowed });
    })
    .catch((error) => {
      console.error("Error:", error.response);
    });
});

router.post("/logout", (req, res) => {
  const envConfig = req.body.envConfig;
  const logoutURL = new URL(
    `${envConfig.authBaseUrl}/auth/realms/${envConfig.realm}/protocol/openid-connect/logout`
  );
  const params = new URLSearchParams();
  params.append("client_id", envConfig.clientId);
  params.append("client_secret", envConfig.clientSecret);
  params.append("refresh_token", refresh_token);

  axios
    .post(logoutURL, params, {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    })
    .then(() => {
      res.json({});
    })
    .catch((error) => {
      console.error("Error:", error.response);
    });
});

router.post("/addtf", (req, res) => {
  console.log("adding a tf...");
  const envConfig = req.body.envConfig;
  const tfPointers = req.body.tfPointers;
  const tfName = req.body.tfName;

  const tfUrl = `${envConfig.tspaBaseUrl}/api/v1/trustframework/${tfName}`;
  axios
    .put(
      tfUrl,
      { scheme: tfPointers },
      {
        headers: {
          Authorization: `Bearer ${bearer_token}`,
        },
      }
    )
    .then((response) => {
      console.log("200 adding a tf ");
      res.status(response.status).json(response.data);
    })
    .catch((error) => {
      if (error.response) {
        res.status(error.response.status).json(error.response.data);
        console.error(">>> Error publishing TF: " + error.response.status);
      } else {
        console.error(">>> TSPA server error no response. ", error);
        const error_data = error.response;
        res.status(500).json({ error_data });
      }
    });
});

router.post("/add-did", (req, res) => {
  console.log("addint a did...");
  const envConfig = req.body.envConfig;
  const did = req.body.did;
  const tfNameForDID = req.body.tfNameForDID;

  const didUrl = `${envConfig.tspaBaseUrl}/api/v1/${tfNameForDID}/trust-list/tsp`;
  axios
    .put(
      didUrl,
      { did },
      {
        headers: {
          Authorization: `Bearer ${bearer_token}`,
        },
      }
    )
    .then((response) => {
      console.log("200 adding a DID");
      res.status(response.status).json(response.data);
    })
    .catch((error) => {
      if (error.response) {
        res.status(error.response.status).json(error.response.data);
      } else {
        const error_data = error.response;
        console.error(">>> Error publishing DID: ", error_data);
        res.status(500).json({ error_data });
      }
    });
});

router.get("/env", (req, res) => {
  res.json(process.env);
});

export default router;
